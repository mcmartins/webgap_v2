/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-05-2014
 *
 */

var configuration = require('@webgap/configuration');

function Account() {
  // always initialize all instance properties
  this.maxRam = configuration.get('MODULE.ACCOUNT.USER_MAX_RAM');
  this.maxSpace = configuration.get('MODULE.ACCOUNT.USER_MAX_SPACE');
  this.maxApps = configuration.get('MODULE.ACCOUNT.USER_MAX_APPS');
  this.maxSlots = configuration.get('MODULE.ACCOUNT.USER_MAX_SLOTS');
  this.active = true;
}

module.exports = Account;