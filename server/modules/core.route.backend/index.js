/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var requireUtils = require('@webgap/require-utils');

module.exports.init = function init() {

  /* other routes for backend are loaded automatically */
  requireUtils.require({
    "require": [
      {
        "directories": [__dirname + '/lib'],
        "pattern": "*.js"
      }
    ]
  }, function (err, ret) {
    if (err) {
      console.error('Error while adding Backend Routes: %s', err);
    } else {
      console.debug('Backend Routes added: %s', ret);
    }
  });
};