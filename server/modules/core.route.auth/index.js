/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var passport = require('passport');
var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator(), Role = Authorizator.Role;
var requireUtils = require('@webgap/require-utils');
var notifierHelper = require('../core.util.notifier.message.helper');
var userService = require('../core.data.service.user');
var SessionUserDTO = require('../core.data.schema').SessionUserDTO;
var template = require('../core.util.template.render');
var utils = require('../core.util.cloud.provider.resolver');

module.exports.init = function init() {

  global.express.app.use(passport.initialize());
  global.express.app.use(passport.session());

  passport.serializeUser(function (profile, done) {
    // the following method handles logged in, existing users not logged in and new users
    userService.insertOrUpdate(profile, function (err, user) {
      if (err) {
        // something is wrong
        return done(err);
      } else {
        // good to go
        return done(null, user);
      }
    });
  });

  passport.deserializeUser(function (user, done) {
    return done(null, new SessionUserDTO(user));
  });

  /* other routes for authentication methods are loaded automatically */
  requireUtils.require({
    "require": [
      {
        "directories": [__dirname + '/lib/'],
        "pattern": "auth-*.js",
        "args": passport
      }
    ]
  }, function (err, ret) {
    if (err) {
      console.error('Error while adding Authentication Routes: %s', err);
    } else {
      console.debug('Authentication Routes added: %s', ret);
    }
  });

  /* terms and conditions acceptance before login */
  global.express.router.get('/auth/login', function (req, res) {
    template.render({
      request: req,
      response: res,
      page: 'landing/login.html',
      data: {
        providers: utils.getActiveProviders(req)
      }
    });
  });

  /* revoke access to account */
  global.express.router.get('/auth/revoke/:provider', authorizator.isAuthorized([Role.USER]), function (req, res) {
    var provider = req.sanitize('provider').toString();
    if (provider) {
      userService.revokeProvider(req.user, provider, function (err) {
        if (err) {
          notifierHelper.ERROR.AUTH_REVOKE_ACCESS_GENERIC({request: req, data: {name: provider}});
        } else {
          notifierHelper.SUCCESS.AUTH_REVOKE_ACCESS({request: req, data: {name: provider}});
        }
        res.redirect('/account/settings');
      });
    } else {
      notifierHelper.ERROR.AUTH_REVOKE_ACCESS_MISSING_PARAM({request: req});
      res.redirect('/account/settings');
    }
  });

  /* logout */
  global.express.router.get('/auth/logout', authorizator.isAuthorized([Role.USER]), function (req, res) {
    req.logout();
    res.redirect('/');
  });
};
