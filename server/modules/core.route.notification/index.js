/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 12-10-2014
 *
 */

var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator(), Role = Authorizator.Role;
var notificationDataService = require('../core.data.service.notification');
var template = require('../core.util.template.render');

// TODO notifications should be presented in real time

module.exports.init = function init() {

  global.express.app.use('/notifications', authorizator.isAuthorized([Role.USER]));

  global.express.router.get('/notifications', function(req, res) {
    notificationDataService.getAll(req.user, function(err, notifications) {
      template.render({
        request: req,
        response: res,
        error: err,
        page: 'backend/notifications/list.html',
        data: {
          notifications: notifications
        }
      });
    });
  });

  global.express.router.get('/notifications/unread', function(req, res) {
    notificationDataService.getUnread(req.user, function(err, notifications) {
      template.render({
        request: req,
        response: res,
        error: err,
        page: 'backend/notifications/list.html',
        data: {
          notifications: notifications
        }
      });
    });
  });
  
  global.express.app.use('/api/notifications', authorizator.isAuthorized([Role.USER]));

  global.express.router.get('/api/notifications', function(req, res) {
    notificationDataService.getAll(req.user, function(err, notifications) {
      if (err) {
        res.sendStatus(401);
      } else {
        res.json(notifications);
      }
    });
  });
  
  global.express.router.post('/api/notifications', function(req, res) {
    var notification = {}; // TODO get body from req
    notification.userId = req.user.id;
    notificationDataService.insert(notification, function(err, res) {
      var status = err ? 500 : 200;
      res.sendStatus(status);
    });
  });
  
  global.express.router.get('/api/notifications/:id', function(req, res) {
    var id = req.sanitize('id').toString();
    notificationDataService.get(req.user, id, function(err, notification) {
      if (err) {
        res.sendStatus(401);
      } else {
        res.json(notification);
      }
    });
  });

  global.express.router.put('/api/notifications/:id', function(req, res) {
    var id = req.sanitize('id').toString();
    notificationDataService.markRead(req.user, id, function(err, res) {
      var status = err ? 500 : 200;
      res.sendStatus(status);
    });
  });

  global.express.router.delete('/api/notifications/:id', function(req, res) {
    var id = req.sanitize('id').toString();
    notificationDataService.remove(req.user, id, function(err, res) {
      var status = err ? 500 : 200;
      res.sendStatus(status);
    });
  });
};
