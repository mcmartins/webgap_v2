/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var callNotifier = require('./util').callNotifier;

module.exports = {
  
  AUTH_REVOKE_ACCESS_GENERIC: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.auth-revoke-access');
  },
  AUTH_REVOKE_ACCESS_MISSING_PARAM: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.auth-revoke-access-missing-param');
  },
  MARKET_APPLICATION_INVALID: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.marketplace-application-invalid');
  },
  MARKET_APPLICATION_CREATE: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.marketplace-application-create');
  },
  MARKET_APPLICATION_DELETE: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.marketplace-application-delete');
  },
  ACCOUNT_SERVICE_ADD: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.account-application-add');
  },
  ACCOUNT_SERVICE_DELETE: function(options) {
    callNotifier(options, ['DATABASE', 'POPUP'], 'messages.error.account-application-delete');
  }
};
