/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var template = require('../core.util.template.render');

module.exports.init = function() {

  console.info('Initializing Routes...');

  /* index */
  global.express.router.get('/', function(req, res) {
    if (req.user) {
      res.redirect('/account/profile');
    } else {
      template.render({
        request: req,
        response: res,
        ignoreLayout: true,
        page: 'landing/index.html'
      });
    }
  });

  /* login */
  require('../core.route.auth').init();

  /* account */
  require('../core.route.backend').init();

  /* landing */
  require('../core.route.landing').init();

  /* notifications */
  require('../core.route.notification').init();
  
  /* status page */
  global.express.router.get('/status', function (req, res) {
    template.render({
      request: req,
      response: res,
      page: 'backend/status/status.html'
    });
  });

  /* 500 server error */
  global.express.router.get('/failure', function(req, res) {
    template.render({
      request: req,
      response: res,
      status: 500,
      error: new Error('Oops, something unexpected just happened...')
    });
  });

  /* 400 unauthorized error */
  global.express.router.get('/bad-request', function(req, res) {
    template.render({
      request: req,
      response: res,
      status: 400,
      error: new Error('Oops, we couldn\'t understand your request...')
    });
  });

  /* 401 unauthorized error */
  global.express.router.get('/unauthorized', function(req, res) {
    template.render({
      request: req,
      response: res,
      status: 401,
      error: new Error('Oops, you\'re not authorized to access this page...')
    });
  });

  /* 404 not found error */
  global.express.router.get('/not-found', function(req, res) {
    template.render({
      request: req,
      response: res,
      status: 404,
      error: new Error('Oops, we couldn\'t find what you\'re looking for...')
    });
  });

  /* 408 timeout */
  global.express.router.get('/timeout', function(req, res) {
    template.render({
      request: req,
      response: res,
      status: 408,
      error: new Error('Oops, your request took more time than expected...')
    });
  });

};
