/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 19-05-2015
 *
 */

var async = require('async');
var DatabaseDAO = require('@webgap/database-dao');

module.exports = {

  insertOrUpdate: function insertOrUpdate(object, callback) {
    var criteria = {id: object.id};
    var self = this;
    async.waterfall([
      function loadActionDAOClient(next) {
        self.actionLogDAO(next);
      },
      function saveActionLog(client, next) {
        client.insert(criteria, function (err) {
          if (err) {
            return callback(err);
          }
          return next(null, object);
        });
      }
    ]);
  },

  // TODO merge this one with the insertOrUpdate
  addActionToLog: function addActionToLog(id, object, callback) {
    var criteria = {id: id};
    var self = this;
    async.waterfall([
      function loadActionDAOClient(next) {
        self.actionLogDAO(next);
      },
      function saveAction(client, next) {
        client.findOne(criteria, function (err, actionLog) {
          if (err) {
            return callback(err);
          }
          if (actionLog) {
            actionLog.history.push(object);
            client.update(criteria, actionLog, function (err) {
              if (err) {
                return callback(err);
              }
              return next(null, object);
            });
          }
        });
      }
    ]);
  },

  get: function get(criteria, callback) {
    this.actionLogDAO(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.findOne(criteria, callback);
    });
  },

  getAll: function getAll(criteria, callback) {
    this.actionLogDAO(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.find(criteria, callback);
    });
  },

  remove: function remove(action, callback) {
    this.actionLogDAO(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.remove({id: action.id}, callback);
    });
  },

  actionLogDAO: function actionLogDAO(callback) {
    DatabaseDAO.getClient('ActionLog', callback);
  }

};
