/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

'use strict';

var async = require('async');
var DatabaseDAO = require('@webgap/database-dao');

module.exports = {

  /**
   * This method handles the notification creation.
   *
   * @param notification
   * @param callback
   */
  insert: function insert(notification, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function createNotification(notificationDAOClient) {
        notificationDAOClient.insert(notification, callback);
      }
    ]);
  },

  /**
   * Returns a specific notification.
   *
   * @param user
   * @param id
   * @param callback
   */
  get: function get(user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function loadNotification(notificationDAOClient) {
        notificationDAOClient.findOne({
          userId: user.id,
          id: id
        }, callback);
      }
    ]);
  },

  /**
   * Returns all unread notifications.
   *
   * @param user
   * @param callback
   */
  getUnread: function getUnread(user, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function loadNotifications(notificationDAOClient) {
        notificationDAOClient.find({
          userId: user.id,
          read: false
        }, callback);
      }
    ]);
  },

  /**
   * Returns all notifications for a specific user.
   *
   * @param user
   * @param callback
   */
  getAll: function getAll(user, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function loadNotifications(notificationDAOClient) {
        notificationDAOClient.find({
          userId: user.id
        }, callback);
      }
    ]);
  },

  /**
   * Remove a notification.
   *
   * @param user
   * @param id
   * @param callback
   */
  remove: function remove(user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function removeNotification(notificationDAOClient) {
        notificationDAOClient.remove({
          userId: user.id,
          id: id
        }, callback);
      }
    ]);
  },

  /**
   * Mark a notifications as read.
   *
   * @param user
   * @param id
   * @param callback
   */
  markRead: function markRead(user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function loadNotificationDAO(next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function markAsRead(notificationDAOClient) {
        notificationDAOClient.findAndModify({userId: user.id, id: id}, {read: true}, callback);
      }
    ]);
  },

  /**
   * Returns the DAO Client
   *
   * @param callback
   * @returns {*}
   */
  getNotificationDAOClient: function getNotificationDAOClient(callback) {
    DatabaseDAO.getClient('Notification', callback);
  }
};
