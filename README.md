# WebGAP Portal

[![Build Status](https://travis-ci.org/webgap/portal.svg)](https://travis-ci.org/webgap/portal)
[![Test Coverage](https://codeclimate.com/github/webgap/portal/badges/coverage.svg)](https://codeclimate.com/github/webgap/portal/coverage)
[![Code Climate](https://codeclimate.com/github/webgap/portal/badges/gpa.svg)](https://codeclimate.com/github/webgap/portal)
[![Dependency Status](https://gemnasium.com/webgap/portal.png)](https://gemnasium.com/webgap/portal)

[![NPM version](http://img.shields.io/npm/v/@webgap/portal.svg?style=flat)](https://www.npmjs.com/package/@webgap/portal)
[![NPM downloads](http://img.shields.io/npm/dm/@webgap/portal.svg?style=flat)](https://www.npmjs.com/package/@webgap/portal)

# README

This is the WebGAP Portal.<br/>
WebGAP rises from the need to provide easier access to [GAP](http://www.gap-system.org/) without installation or maintenance required.
WebGAP is a simple web interface to run GAP on the cloud over web shell. This functionality has been extended to allow the access to other mathematical tools, such as Prover9.


# Dependencies

Check *package.json*.<br/>

# Requirements

A configuration file must exist
(check [**@webgap/configuration**](https://github.com/webgap/configuration) documentation).

# API

### Installation

```bash
npm install @webgap/portal --save
```

### Usage

# License

Apache License, Version 2.0