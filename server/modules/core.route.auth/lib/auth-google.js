/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var googleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configuration = require('@webgap/configuration');

module.exports = function (passport) {

  var strategy = new googleStrategy({
      clientID: configuration.get('MODULE.AUTHENTICATION.GOOGLE.CLIENT_ID'),
      clientSecret: configuration.get('MODULE.AUTHENTICATION.GOOGLE.CLIENT_SECRET'),
      callbackURL: [configuration.get('SERVER.PROTOCOL'), configuration.get('SERVER.HOST'), ':',
        configuration.get('SERVER.APACHE_PORT'), '/auth/google/oauth2callback'].join(''),
      passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
      process.nextTick(function () {
        profile.accessToken = accessToken || null;
        profile.tokenType = null;
        profile.expiresIn = null;
        profile.refreshToken = refreshToken || null;
        profile.loggedUser = req.user || null;
        profile.cloudStorage = true;
        return done(null, profile);
      });
    }
  );

  strategy.cloudStorage = true;

  passport.use(strategy);

  global.express.router.get('/auth/google', passport.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email']
  }));

  global.express.router.get('/auth/google/oauth2callback', passport.authenticate('google', {failureRedirect: '/'}),
    function (req, res) {
      res.redirect('/account/profile');
    });

};