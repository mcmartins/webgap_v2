/**
 * Created by Manuel on 18/11/2015.
 */

/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

module.exports = {

  getActiveProviders: function getActiveCloudProviders(req) {
    var strategies = req._passport.instance._strategies;
    var providers = [];
    for (var strategy in strategies) {
      if (strategies.hasOwnProperty(strategy)) {
        //ignore local session strategy
        if (strategy === 'session') {
          continue;
        }
        var provider = {};
        // remove extra oauth etc info using split
        provider.name = strategy.split("-")[0];
        provider.cloudStorage = strategies[strategy].cloudStorage;
        providers.push(provider);
      }
    }
    return providers;
  }
};
