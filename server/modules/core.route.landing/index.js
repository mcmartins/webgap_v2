/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var template = require('../core.util.template.render');

module.exports.init = function init() {

  /**
   * disclaimer
   */
  global.express.router.get('/disclaimer', function(req, res) {
    template.render({
      request: req,
      response: res,
      page: 'landing/doc/disclaimer.html'
    });
  });

  /**
   * terms
   */
  global.express.router.get('/terms', function(req, res) {
    template.render({
      request: req,
      response: res,
      page: 'landing/doc/terms.html'
    });
  });

};