/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var BitbucketStrategy = require('passport-bitbucket').Strategy;
var configuration = require('@webgap/configuration');

module.exports = function (passport) {

  var strategy = new BitbucketStrategy({
      consumerKey: configuration.get('MODULE.AUTHENTICATION.BITBUCKET.CLIENT_ID'),
      consumerSecret: configuration.get('MODULE.AUTHENTICATION.BITBUCKET.CLIENT_SECRET'),
      callbackURL: [configuration.get('SERVER.PROTOCOL'), configuration.get('SERVER.HOST'), ':',
        configuration.get('SERVER.APACHE_PORT'), '/auth/bitbucket/return'].join(''),
      passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
      process.nextTick(function () {
        profile.accessToken = accessToken || null;
        profile.id = profile.username;
        profile.tokenType = null;
        profile.expiresIn = null;
        profile.refreshToken = refreshToken || null;
        profile.loggedUser = req.user || null;
        return done(null, profile);
      });
    }
  );

  passport.use(strategy);

  global.express.router.get('/auth/bitbucket', passport.authenticate('bitbucket'));

  global.express.router.get('/auth/bitbucket/return', passport.authenticate('bitbucket', {failureRedirect: '/auth/login'}),
    function (req, res) {
      res.redirect('/account/profile');
    });

};