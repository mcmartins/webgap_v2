/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 11-12-2015
 *
 */

var _ = require('underscore');
var async = require('async');
var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator();
var Formatter = require('@webgap/format-utils');
var formatter = new Formatter();
// fixed variables
var LAYOUT = 'layout.html';

/**
 * Check for messages to use as templating context
 */
function getMessage(req) {
  var message = undefined;
  if (req._passport.session) {
    message = req._passport.session.message;
    // delete messages from session to prevent loading them on next request
    req.session.message = undefined;
  }
  return message;
}


/**
 * Template rendering utility methods
 */
module.exports = {

  /**
   * Non blocking render.
   *
   * {request: express.req, response: express.res, error: Error, page: "pageName", ignoreLayout: false, data: {var1: value, var2: value}}
   */
  render: function (options) {
    options = options || {};
    var req = options.request;
    var res = options.response;
    var err = options.error;
    var status = options.status || err ? 500 : 200;
    async.series([
      async.asyncify(
        function renderLayoutPage() {
          // create template context
          var context = {
            version: '', // TODO get it from portal package
            url: req.originalUrl,
            page: options.page,
            user: _.clone(req.user),
            message: getMessage(req),
            authorizator: authorizator,
            formatter: formatter
          };
          // if error is present should show the error
          if (err) {
            // replace page with server error default with error message
            context.page = 'common/' + status + '.html';
            context.data = err.message;
            res.status(status).render(LAYOUT, context);
          } else {
            // add data to context
            for (var key in options.data) {
              if (options.data.hasOwnProperty(key)) {
                context[key] = options.data[key];
              }
            }
            // should we render just the page? Or the whole layout?
            if (options.ignoreLayout) {
              res.status(status).render(options.page, context);
            } else {
              res.status(status).render(LAYOUT, context);
            }
          }
        }
      )
    ]);
  }
};