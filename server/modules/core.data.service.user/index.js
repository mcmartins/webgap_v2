/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

'use strict';

var async = require('async');
var _ = require('underscore');
var marketService = require('../core.data.service.market');
var entities = require('../core.data.schema');
var configuration = require('@webgap/configuration');
var DatabaseDAO = require('@webgap/database-dao');

module.exports = {

  /**
   * This method handles the user creation by checking:
   * if the user is logged in should create/update the identity
   * otherwise should create the user
   *
   * @param profile
   * @param callback
   */
  insertOrUpdate: function insertOrUpdate(profile, callback) {
    var self = this;
    async.waterfall([
      // load user client DAO
      function loadUserDAO(next) {
        self.getUserDAOClient(next);
      },
      // handle user load from database
      function loadUser(userDAOClient, next) {
        // generate identity
        var identity = new entities.Identity(profile.id, profile.provider,
          new entities.IdentityToken(profile.accessToken, profile.tokenType, profile.expiresIn,
            profile.refreshToken));
        var loggedUser = profile.loggedUser;
        // TODO performance improvement - load
        async.waterfall([
          // load user from database
          function loadUserByloggedUserEmail(nextInternal) {
            // check if there is a user registered with the main email for the current profile
            userDAOClient.findOne({
              email: loggedUser ? loggedUser.email : null
            }, function (err, user) {
              if (err) {
                return callback(err);
              }
              if (user) {
                return next(null, userDAOClient, identity, user);
              } else {
                return nextInternal(null);
              }
            });
          },
          function loadUserByUserEmail(nextInternal) {
            // check if there is a user registered with the main email for the current profile
            userDAOClient.findOne({
              email: profile.emails ? profile.emails[0].value : null
            }, function (err, user) {
              if (err) {
                return callback(err);
              }
              if (user) {
                return next(null, userDAOClient, identity, user);
              } else {
                return nextInternal(null);
              }
            });
          },
          function loadUserByUserIdentity() {
            // check if there is a user registered with the main email for the current profile
            userDAOClient.findOne({
              identities: {
                $elemMatch: {
                  id: identity.id,
                  provider: identity.provider
                }
              }
            }, function (err, user) {
              if (err) {
                return callback(err);
              }
              if (user) {
                return next(null, userDAOClient, identity, user);
              } else {
                return next(null, userDAOClient, identity, undefined);
              }
            });
          }
        ]);
      },
      // check if user is logged in and if the identity has to be added or updated
      function saveOrUpdateIdentity(userDAOClient, identity, dbUser, next) {
        // if the identity is associated update the user identity
        var index;
        // check the index of the identity if it exists
        if (dbUser) {
          index = _.findIndex(dbUser.identities, {id: identity.id});
          if (index >= 0) {
            dbUser.identities.splice(index, 1);
            dbUser.identities.push(identity);
            dbUser.photo = profile.photos ? profile.photos[0].value : dbUser.photo;
            userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
              // return error
              if (err) {
                return callback(err);
              }
            });
            return callback(null, dbUser);
            // if there is a logged user and the identity is not present update the user
          } else {
            dbUser.identities.push(identity);
            userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
              // return error
              if (err) {
                return callback(err);
              }
            });
            return callback(null, dbUser.id);
          }
        } else {
          return next(null, userDAOClient, identity);
        }
      },
      // seems the user is not registered, create and persist it
      function saveUser(userDAOClient, identity) {
        var user = new entities.User(profile.displayName);
        user.email = profile.emails ? profile.emails[0].value : null;
        user.photo = profile.photos ? profile.photos[0].value : null;
        user.identities.push(identity);
        userDAOClient.insert(user, function (err) {
          // return error
          if (err) {
            return callback(err);
          }
        });
        return callback(null, user);
      }
    ]);
  },

  /**
   * Deletes an user
   *
   * @param user the user to delete
   * @param callback
   */
  remove: function remove(user, callback) {
    // if the user is an admin or the logged user, proceed with deletion
    // copy to userhistory
    // TODO implement this method
    callback(new Error('Not implemented!'));
  },

  /**
   * Revoke access to provider
   *
   * @param user the user in session
   * @param provider the provider to revoke access
   * @param callback
   */
  revokeProvider: function revokeProvider(user, provider, callback) {
    var self = this;
    if (!user) {
      return callback(new Error('The user is not defined!'));
    }
    async.waterfall([
      // load user client DAO
      function loadUserDAO(next) {
        self.getUserDAOClient(next);
      },
      // load user
      function loadUser(userDAOClient, next) {
        var index;
        // check if there is a user registered with the main email for the current profile
        async.waterfall([
          // load user from database
          function loadUserFromDB(initializer) {
            userDAOClient.findOne({
              email: user ? user.id : null
            }, initializer);
          },
          function getIndexOfIdentityByProvider(dbUser) {
            // check the index of the identity if it exists
            if (dbUser) {
              index = _.findIndex(dbUser.identities, {provider: provider});
            }
            // proceed
            return next(null, userDAOClient, dbUser, index);
          }
        ]);
      },
      // check if identity exists, delete it and update the user
      function removeIdentityAndUpdateUser(userDAOClient, dbUser, index) {
        if (index >= 0) {
          dbUser.identities.splice(index, 1);
          userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
            // return error
            if (err) {
              return callback(err);
            }
          });
          return callback();
        } else {
          return callback(new Error('The provider is not associated to the user!'));
        }
      }
    ]);
  },

  /**
   * Subscribe access to application
   *
   * @param user the user in session
   * @param appId the application ID
   * @param callback
   */
  addApplication: function addApplication(user, appId, callback) {
    var self = this;
    async.waterfall([
      // load user client DAO
      function loadUserDAO(next) {
        self.getUserDAOClient(next);
      },
      function loadUserAndApplication(userDAOClient, next) {
        async.parallel({
          // load user from database
          user: function (initializer) {
            userDAOClient.findOne({id: user.id}, function (err, user) {
              if (err) {
                return callback(err);
              }
              if (user) {
                return initializer(null, user);
              } else {
                return callback(new Error('Invalid User!'));
              }
            });
          },
          // load application from database
          application: function (initializer) {
            marketService.getApplication({id: appId}, function (err, application) {
              if (err) {
                return callback(err);
              }
              if (application) {
                return initializer(null, application);
              } else {
                return callback(new Error('Invalid Application!' + appId));
              }
            });
          }
        }, function initializer(err, results) {
          if (err) {
            return callback(err);
          }
          return next(null, userDAOClient, results.user, results.application);
        });
      },
      function associateApplicationToUser(userDAOClient, user, application) {
        // check if user can have more apps
        if (user.applications.length > configuration.get('MODULE.ACCOUNT.USER_MAX_APPS')) {
          return callback(new Error('User cannot associate more applications!'));
        }
        // check if user has already access to the application
        async.waterfall([
          function checkApplication(next) {
            if (user.applications.some(function (applicationId) {
                return applicationId === application.id;
              })) {
              var error = new Error('Application ' + application.name + ' is already associated with the user!');
              error.name = application.name;
              return callback(error);
            } else {
              return next();
            }
          },
          function addApplication() {
            // add application and update user
            user.applications.push(application.id);
            userDAOClient.update({id: user.id}, user, function (err) {
              if (err) {
                return callback(err);
              }
            });
            return callback(null, application.name);
          }
        ]);
      }
    ]);
  },

  /**
   * Remove application subscription
   *
   * @param user the user in session
   * @param appId application ID
   * @param callback
   */
  removeApplication: function removeApplication(user, appId, callback) {
    var self = this;
    async.waterfall([
      // load user client DAO
      function loadUserDAO(next) {
        self.getUserDAOClient(next);
      },
      function loadUserAndApplication(userDAOClient, next) {
        async.parallel({
          // load user from database
          user: function loadUser(initializer) {
            userDAOClient.findOne({id: user.id}, function (err, user) {
              if (err) {
                return callback(err);
              }
              if (user) {
                return initializer(null, user);
              } else {
                return callback(new Error('Invalid User!'));
              }
            });
          },
          // load application from database
          application: function loadApplication(initializer) {
            marketService.getApplication({id: appId}, function (err, application) {
              if (err) {
                return callback(err);
              }
              if (application) {
                return initializer(null, application);
              } else {
                return callback(new Error('Invalid Application!' + appId));
              }
            });
          }
        }, function initializer(err, results) {
          if (err) {
            return callback(err);
          }
          return next(null, userDAOClient, results.user, results.application);
        });
      },
      function removeApplicationFromUser(userDAOClient, user, application) {
        // check if user has access to the application
        async.detect(user.applications,
          function checkApplication(applicationId, handle) {
            if (applicationId === application.id) {
              return handle(application);
            }
          },
          function handle(result) {
            if (!result) {
              return callback(new Error('Application ' + application.name + 'is not associated with the user!'));
            }
            var index = user.applications.indexOf(application.id);
            // no risk because we check that the user has access to the application before
            user.applications.splice(index, 1);
            userDAOClient.update({id: user.id}, user, function (err) {
              if (err) {
                return callback(err);
              }
              return callback();
            });
          }
        );
      }
    ]);
  },

  getApplications: function getApplications(user, callback) {
    var applications = [];
    async.each(user.applications,
      function handle(appId, next) {
        marketService.getApplication({id: appId}, function (err, application) {
          if (err) {
            return callback(err);
          }
          applications.push(application);
          return next();
        });
      }, function (err) {
        if (err) {
          return callback(err);
        }
        return callback(null, applications);
      }
    );
  },

  /**
   * Returns a user based on his identity
   *
   * @param identity the identity
   * @param callback
   * @returns {*} the user
   */
  getUserByIdentity: function getUserByIdentity(identity, callback) {
    this.getUserDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.findOne({
        identities: {
          $elemMatch: {
            id: identity.id,
            provider: identity.provider
          }
        }
      }, callback);
    });
  },

  /**
   * Returns a user based on his identity
   *
   * @param id the id
   * @param callback
   * @returns {*} the user
   */
  getUserById: function getUserById(id, callback) {
    this.getUserDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.findOne({
        id: id
      }, callback);
    });
  },

  /**
   * Returns a user based on his email
   *
   * @param email the email
   * @param callback
   * @returns {*} the user
   */
  getUserByEmail: function getUserByEmail(email, callback) {
    this.getUserDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.findOne({email: email}, callback);
    });
  },

  /**
   * Returns all users matching a criteria
   * If no criteria is provided returns all
   *
   * @param criteria the criteria
   * @param callback
   * @returns {*} the list of users
   */
  getAllUsers: function getAllUsers(criteria, callback) {
    this.getUserDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.find(criteria, callback);
    });
  },

  /**
   * Returns the DAO Client
   *
   * @param callback
   * @returns {*}
   */
  getUserDAOClient: function getUserDAOClient(callback) {
    DatabaseDAO.getClient('User', callback);
  },

  /**
   * Returns the DAO Client
   *
   * @param callback
   * @returns {*}
   */
  getHistoryUserDAOClient: function getHistoryUserDAOClient(callback) {
    DatabaseDAO.getClient('HistoryUser', callback);
  }

};
