/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 02-05-2015
 *
 */

var PopupSystem = require('./lib/popup');
var EmailSystem = require('./lib/email');
var DatabaseSystem = require('./lib/database');
var notifier = require('@webgap/notifier');

module.exports.init = function init() {
  notifier.register({name: PopupSystem.NAME, instance: PopupSystem, defaultSystem: true});
  notifier.register({name: EmailSystem.NAME, instance: EmailSystem, defaultSystem: false});
  notifier.register({name: DatabaseSystem.NAME, instance: DatabaseSystem, defaultSystem: false});
};