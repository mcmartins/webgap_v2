/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-05-2014
 *
 */

var util = require('util');
var Role = require('@webgap/authorization-utils').Role;
var HatAlgorithm = require('@webgap/token').AuthToken.HatAlgorithm;
var token = new HatAlgorithm();
var AbstractEntity = require('./../AbstractEntity');
var languageEnum = require('./Language');
var Account = require('./Account');

function User(name) {
  User.super_.apply(this);
  // always initialize all instance properties
  if (!name) {
    throw new Error('Name cannot be empty!');
  }
  this.name = name;
  this.email = null;
  this.photo = null;
  this.language = languageEnum.EN_US;
  this.token = token.encode();
  this.roles = [];
  this.roles.push(Role.USER);
  // TODO remove extra roles after implementing the administration area for user management
  this.roles.push(Role.ADMIN);
  this.roles.push(Role.PROVIDER);
  this.groups = [];
  this.organisations = [];
  this.identities = [];
  this.applications = [];
  this.account = new Account();
}

/* inherit AbstractEntity Module */
util.inherits(User, AbstractEntity);

module.exports = User;