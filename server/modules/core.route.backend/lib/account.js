/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator(), Role = Authorizator.Role;
var providersUtils = require('../../core.util.cloud.provider.resolver');
var notifierHelper = require('../../core.util.notifier.message.helper');
var userDataService = require('../../core.data.service.user');
var UserDTO = require('../../core.data.schema').SessionUserDTO;
var formBuilder = require('../../core.util.alpaca.build');
var template = require('../../core.util.template.render');
var async = require('async');

/**
 * User Account Routes
 */
module.exports = function () {

  global.express.app.use('/account', authorizator.isAuthorized([Role.USER]));

  /* profile */
  global.express.router.get('/account/profile/:id*?', function (req, res) {
    var id = req.sanitize('id').toString() || req.user.id;
    // TODO should be loaded with waterfall and then parallel
    userDataService.getUserById(id, function afterLoadUser(err, user) {
      async.parallel({
        applications: function getApplications(next) {
          if (!user) {
            return next(new Error('User not found!'));
          }
          userDataService.getApplications(user, next);
        }
        // TODO load social data, activity, etc
      }, function render(err, result) {
        template.render({
          request: req,
          response: res,
          error: err,
          page: 'backend/account/profile.html',
          data: {
            profile: user,
            applications: result.applications
          }
        });
      });
    });
  });

  /* TODO REMOVE - create new routes and add token timming validation */
  global.express.router.get('/internal/users/list', function (req, res) {
    userDataService.getAllUsers(null, function (err, users) {
      var u = {};
      users.forEach(function (user) {
        u[user.id] = user.name + '(' + user.email + ')';
        res.json(u);
      });
    });
  });

  /* terminal */
  global.express.router.get('/account/terminal', function (req, res) {
    template.render({
      request: req,
      response: res,
      page: 'backend/services/terminal.html'
    });
  });

  /* settings */
  global.express.router.get('/account/settings', function (req, res) {
    userDataService.getUserById(req.user.id, function afterLoadUser(err, user) {
      template.render({
        request: req,
        response: res,
        error: err,
        page: 'backend/account/settings.html',
        data: {
          providers: providersUtils.getActiveProviders(req),
          form: formBuilder.build("user.json", new UserDTO(user), '/account/settings/save')
        }
      });
    });
  });

  /* settings save */
  global.express.router.post('/account/settings/save', function (req, res) {
    // TODO implement this
    template.render({
      request: req,
      response: res,
      page: 'backend/account/settings.html',
      data: {
        providers: providersUtils.getActiveCloudProviders(req),
        form: formBuilder.build("user.json", new SessionUserDTO(req.user), '/account/settings/save')
      }
    });
  });

  /* add app */
  global.express.router.get('/account/applications/add/:id', function (req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    userDataService.addApplication(req.user, id, function (err, result) {
      if (err) {
        console.error(err);
        notifierHelper.ERROR.ACCOUNT_SERVICE_ADD({request: req, data: {name: err.name}});
      } else {
        notifierHelper.SUCCESS.ACCOUNT_SERVICE_ADD({request: req, data: {name: result}});
      }
      // redirect to the origin url
      res.redirect(req.header('Referer'));
    });
  });

  /* remove app */
  global.express.router.get('/account/applications/remove/:id', function (req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    userDataService.removeApplication(req.user, id, function (err) {
      if (err) {
        console.error(err);
        notifierHelper.ERROR.ACCOUNT_SERVICE_DELETE({request: req, data: {name: err.name}});
      } else {
        notifierHelper.SUCCESS.ACCOUNT_SERVICE_DELETE({request: req, data: {name: err.name}});
      }
      // redirect to the origin url
      res.redirect(req.header('Referer'));
    });
  });

  /* workspace */
  global.express.router.get('/account/workspace', function (req, res) {
    template.render({
      request: req,
      response: res,
      page: 'backend/services/workspace.html'
    });
  });

  /* ide */
  global.express.router.get('/account/ide', function (req, res) {
    template.render({
      request: req,
      response: res,
      page: 'backend/services/ide.html'
    });
  });

  /* sync service */
  global.express.router.get('/account/:service/sync', function (req, res) {
    // TODO re-implement sync services
    var service = req.sanitize('service').toString();
    var templateVariables = template.generate(req);
    templateVariables.terminalUrl = "https://instance01.webgap.eu/service/" + service;
    res.render('backend/services/terminal.html', templateVariables);
  });

  /* async service */
  global.express.router.get('/account/:service/async', function (req, res) {
    // TODO implement async services
  });

};