/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 11-04-2015
 *
 */

"use strict";

var Action = require('./Action'),
  uuid = require('node-uuid');

function ActionLog(options) {
  // always initialize all instance properties
  this.id = uuid.v4();
  this.userId = options.user && options.user.id;
  this.name = options.name;
  this.data = options.data;
  this.type = options.type;
  this.history = [];
  this.history.push(new Action(options));
}

module.exports = ActionLog;

module.exports.Type = {
  CREATE: 'created',
  DELETE: 'deleted',
  UPDATE: 'updated',
  UPLOAD: 'uploaded',
  DOWNLOAD: 'downloaded'
};