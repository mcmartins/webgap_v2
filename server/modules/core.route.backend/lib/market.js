/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var notifierHelper = require('../../core.util.notifier.message.helper');
var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator(),
  Role = Authorizator.Role;
var template = require('../../core.util.template.render');
var marketDataService = require('../../core.data.service.market');
var formBuilder = require('../../core.util.alpaca.build');
var formidable = require('formidable');
var uuid = require('node-uuid');

/**
 * Applications Market routes
 */
module.exports = function() {

  global.express.app.use('/marketplace', authorizator.isAuthorized([Role.USER]));

  /* marketplace */
  global.express.router.get('/marketplace', function(req, res) {
    marketDataService.getAll(null, function(err, applications) {
      template.render({
        request: req,
        response: res,
        page: 'backend/marketplace/marketplace.html',
        data: {
          applications: applications
        }
      });
    });
  });

  /* applications */
  global.express.router.get('/marketplace/applications/list?', function(req, res) {
    var query = req.sanitizeQuery('q').toString();
    // TODO ASSESS security - possible injection
    var criteria;
    if (query) {
      criteria = {
        $or: [{
          tags: {
            $elemMatch: {
              $regex: new RegExp("^" + query.toLowerCase(), "i")
            }
          }
        }, {
          categories: {
            $elemMatch: {
              $regex: new RegExp("^" + query.toLowerCase(), "i")
            }
          }
        }, {
          name: {
            $regex: new RegExp("^" + query.toLowerCase(), "i")
          }
        }, {
          description: {
            $regex: new RegExp("^" + query.toLowerCase(), "i")
          }
        }]
      };
    }
    marketDataService.getAll(criteria, function(err, applications) {
      template.render({
        request: req,
        response: res,
        error: err,
        page: 'backend/marketplace/list.html',
        ignoreLayout: true,
        data: {
          applications: applications,
          query: query
        }
      });
    });
  });

  /* create new application */
  global.express.router.get('/marketplace/applications/new', authorizator.isAuthorized([Role.ADMIN, Role.PROVIDER]), function(req, res) {
    template.render({
      request: req,
      response: res,
      page: 'backend/marketplace/add.html',
      data: {
        form: formBuilder.build("application.json", {
          id: uuid.v4(), // new id for each app
          owner: {
            id: req.user.id,
            name: req.user.name
          }
        }, '/marketplace/applications/save')
      }
    });
  });

  /* edit application */
  // TODO use post instead of get for saving
  global.express.router.get('/marketplace/applications/update/:id', authorizator.isAuthorized([Role.ADMIN, Role.PROVIDER]), function(req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    marketDataService.get({
      id: id
    }, function(err, application) {
      if (application) {
        var requestVariables = template.generate(req);
        requestVariables.form = formBuilder.build("application.json", application, '/marketplace/applications/save');
        res.render('backend/marketplace/add.html', requestVariables);
      } else {
        notifierHelper.INFO.MARKET_APPLICATION_INVALID({
          request: req
        });
        // redirect to the origin url
        res.redirect(req.header('Referer'));
      }
    });
  });

  /* remove application */
  // TODO use delete instead of get for deleting
  global.express.router.get('/marketplace/applications/delete/:id', authorizator.isAuthorized([Role.ADMIN, Role.PROVIDER]), function(req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    marketDataService.get({
      id: id
    }, function(err, application) {
      if (application) {
        // TODO delete only if the user is admin or the owner of the application
        marketDataService.deleteApplication(application, function() {});
        notifierHelper.INFO.MARKET_APPLICATION_DELETE({
          request: req
        });
      } else {
        notifierHelper.ERROR.MARKET_APPLICATION_DELETE({
          request: req
        });
      }
      // redirect to the origin url
      res.redirect(req.header('Referer'));
    });
  });

  /* add or update application */
  global.express.router.post('/marketplace/applications/save', authorizator.isAuthorized([Role.ADMIN, Role.PROVIDER]), function(req, res) {
    // TODO ASSESS security - possible injection
    // TODO validate against the JSON Schema
    marketDataService.insertOrUpdate(req.body, function(err) {
      if (err) {
        notifierHelper.ERROR.MARKET_APPLICATION_CREATE({
          request: req
        });
      } else {
        notifierHelper.SUCCESS.MARKET_APPLICATION_CREATE({
          request: req
        });
      }
    });
    notifierHelper.INFO.MARKET_APPLICATION_CREATE({
      request: req
    });
    res.redirect('/marketplace');
  });

  /* add or update application */
  global.express.router.post('/marketplace/applications/upload', authorizator.isAuthorized([Role.ADMIN, Role.PROVIDER]), function(req, res) {
    //TODO add options to configurations
    var form = new formidable.IncomingForm({
      uploadDir: "/tmp",
      keepExtensions: true,
      maxFieldsSize: 200 * 1024 * 1024,
      maxFields: 1
    });
    form.on('fileBegin', function(name, file) {
      console.log("debug", "receiving file path json " + name + " - " + JSON.stringify(file));
    });
    form.parse(req, function(err, fields, files) {
      if (err) {
        console.error('uploading files: ' + JSON.stringify(files) + '.' + err);
        //res.status(500).send();
      } else {
        console.info("uploaded files: " + JSON.stringify(files));
        //res.status(200).send();
        // TODO handle uploaded files
      }
    });
  });

};