/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var Authorizator = require('@webgap/authorization-utils');
var authorizator = new Authorizator(), Role = Authorizator.Role;
var configuration = require('@webgap/configuration');
var userService = require('../../core.data.service.user');
var template = require('../../core.util.template.render');

/**
 * Admin Routes
 */
module.exports = function () {

  global.express.app.use('/admin', authorizator.isAuthorized([Role.ADMIN]));

  /* admin */
  global.express.router.get('/admin', function (req, res) {
    res.render('backend/admin/admin.html', template.generate(req));
  });

  /* users */
  global.express.router.get('/admin/users', function (req, res) {
    userService.getAllUsers(function (err, users) {
      var variables = template.generate(req);
      variables.users = users;
      res.render('backend/admin/users.html', variables);
    });
  });

  /* stats */
  global.express.router.get('/admin/stats', function (req, res) {
    // TODO implement stats
  });

  /* accounts */
  global.express.router.get('/admin/accounts', function (req, res) {
    var variables = template.generate(req);
    variables.accounts = [];
    function loadAccounts(key, val) {
      variables.accounts.push(JSON.parse(val));
    }

    //accountService.getAllAccounts(loadAccounts);
    res.render('backend/admin/accounts.html', variables);
  });

  /* configurations */
  global.express.router.get('/admin/configurations', function (req, res) {
    var variables = template.generate(req);
    variables.configurations = JSON.stringify(configuration);
    res.render('backend/admin/configurations.html', variables);
  });

  /* maintenance */
  global.express.router.get('/admin/maintenance', function (req, res) {
    res.render('backend/admin/maintenance.html', template.generate(req));
  });

  /* tools */
  global.express.router.get('/admin/tools', function (req, res) {
    res.render('backend/admin/tools.html', template.generate(req));
  });

};