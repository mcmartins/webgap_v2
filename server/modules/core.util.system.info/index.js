/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-03-2015
 *
 */

var CPUUtils = require('./lib/cpu.js');
var MemoryUtils = require('./lib/memory.js');
var PlatformUtils = require('./lib/platform.js');
var async = require('async');

module.exports = function getStatus(callback) {
  async.parallel({
    CPULoadAverage: function(next) {
      async.parallel([
        function(next) {
          CPUUtils.getLoadAverage(1, next);
        },
        function(next) {
          CPUUtils.getLoadAverage(5, next);
        },
        function(next) {
          CPUUtils.getLoadAverage(15, next);
        }
      ], next);
    },
    CPUUsage: function(next) {
      // callback from os-utils does not follow the err, val signature
      CPUUtils.getCPUUsage(function(val) {
        return next(null, val);
      });
    },
    CPUFree: function(next) {
      // callback from os-utils does not follow the err, val signature
      CPUUtils.getCPUFree(function(val) {
        return next(null, val);
      });
    },
    MemoryTotal: function(next) {
      MemoryUtils.getTotalMemory(next);
    },
    MemoryFree: function(next) {
      MemoryUtils.getFreeMemory(next);
    },
    PlatformUpTime: function(next) {
      PlatformUtils.getUpTime(next);
    },
    PlatformName: function(next) {
      PlatformUtils.getPlatform(next);
    }
  }, callback);
};
module.exports.CPUUtils = CPUUtils;
module.exports.MemoryUtils = MemoryUtils;
module.exports.PlatformUtils = PlatformUtils;