/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var util = require('util');
var WebModule = require('./modules/core.module.web');
var configuration = require('@webgap/configuration');
var favicon = require('serve-favicon');
var session = require('express-session');
var ejs = require('ejs');
var i18n = require('i18n');
var express = require('express');
var async = require('async');

/**
 * WebGAP Portal initialize scripts
 */
function WebGAPPortal() {
  WebGAPPortal.super_.apply(this, arguments);

  console.info('Initializing Portal...');

  // express general configuration
  global.express.app.engine('.html', ejs.__express);
  global.express.app.set('view engine', 'html');
  global.express.app.set('views', __dirname + '/../client');
  global.express.app.use(express.static(__dirname + '/../public', {maxAge: 10886400000}));
  global.express.app.use(favicon(__dirname + '/../public/img/favicon.ico'));
  global.express.app.use(i18n.init);

  // passport session is initialized inside the module 'core.route.auth' called by the router
  global.express.app.use(session({
      secret: configuration.get('GENERAL.COOKIE.SECRET'),
      maxAge: new Date(Date.now() + configuration.get('GENERAL.COOKIE.MAX_AGE')),
      cookie: {
        httpOnly: true,
        secure: true
      },
      key: 'WebGAP-lYGCXX656Z0Sb99zo935qyaCAp7a250F.sid',
      saveUninitialized: true,
      resave: true
    })
  );

  /* initialize internationalization */
  i18n.configure({
    locales: ['pt', 'en'],
    defaultLocale: 'en',
    directory: __dirname + '/../public/locales',
    updateFiles: false,
    objectNotation: true
  });

  // TODO add module validation for params router.param('id', /^\d+$/); uuid

  /* initialize publisher subscriber */
  //require('./modules/common.amqp.client');

  async.parallel([
    function loadRoutes(next) {
      /* initialize router */
      require('./modules/core.route').init();
      return next();
    },
    function loadNotifierSystems(next) {
      /* initialize the notifications system */
      require('./modules/core.util.notifier.system').init();
      return next();
    }
  ], function afterLoad(err){
    if (err) {
      throw err;
    }
  });
}

/* inherit Web Module */
util.inherits(WebGAPPortal, WebModule);

module.exports = WebGAPPortal;