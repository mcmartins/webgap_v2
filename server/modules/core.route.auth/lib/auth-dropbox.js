/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var dropboxStrategy = require('passport-dropbox-oauth2').Strategy;
var configuration = require('@webgap/configuration');

module.exports = function (passport) {

  var strategy = new dropboxStrategy({
      clientID: configuration.get('MODULE.AUTHENTICATION.DROPBOX.CLIENT_ID'),
      clientSecret: configuration.get('MODULE.AUTHENTICATION.DROPBOX.CLIENT_SECRET'),
      callbackURL: [configuration.get('SERVER.PROTOCOL'), configuration.get('SERVER.HOST'), ':',
        configuration.get('SERVER.APACHE_PORT'), '/auth/dropbox/oauth2callback'].join(''),
      passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
      process.nextTick(function () {
        profile.accessToken = accessToken || null;
        profile.tokenType = null;
        profile.expiresIn = null;
        profile.refreshToken = refreshToken || null;
        profile.loggedUser = req.user || null;
        profile.cloudStorage = true;
        return done(null, profile);
      });
    }
  );

  strategy.cloudStorage = true;

  passport.use(strategy);

  global.express.router.get('/auth/dropbox', passport.authenticate('dropbox-oauth2'));

  global.express.router.get('/auth/dropbox/oauth2callback', passport.authenticate('dropbox-oauth2', {failureRedirect: '/auth/login'}),
    function (req, res) {
      res.redirect('/account/profile');
    });

};