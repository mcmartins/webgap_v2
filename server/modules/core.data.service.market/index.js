/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 12-10-2014
 *
 */

var async = require('async');
var Audit = require('../core.data.schema').Audit;
var DatabaseDAO = require('@webgap/database-dao');

module.exports = {

  insertOrUpdate: function insertOrUpdate(object, callback) {
    var criteria = {id: object.id};
    var self = this;
    async.waterfall([
      // load marketplace client DAO
      function loadMarketDAOClient(next) {
        self.marketDAOClient(next);
      },
      // check if application already exists - save or update
      function saveOrUpdateApplication(client, next) {
        client.findOne(criteria, function (err, application) {
          if (err) {
            return callback(err);
          }
          if (application) {
            var date = application.audit ? application.audit.createDate : null;
            object.audit = new Audit(date);
            client.update(criteria, object, function (err) {
              if (err) {
                return callback(err);
              }
              return next(null, object);
            });
          } else {
            object.audit = new Audit();
            client.insert(object, function (err) {
              if (err) {
                return callback(err);
              }
              return next(null, object);
            });
          }
        });
      },
      // handle server tasks
      function dockeri(application) {
        // TODO update service and run background tasks (docker etc)
        //
        // generate service object from application
        //
        // handle uploaded files - unpack / change permissions / store it
        //
        // create new docker image to run the software - https://github.com/docker/docker
        //
        // manage data volumes - https://docs.docker.com/userguide/dockervolumes/
      }
    ]);
  },

  get: function get(criteria, callback) {
    this.marketDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.findOne(criteria, callback);
    });
  },

  getAll: function getAll(criteria, callback) {
    this.marketDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.find(criteria, callback);
    });
  },

  remove: function remove(application, callback) {
    this.marketDAOClient(function (err, client) {
      if (err) {
        return callback(err);
      }
      client.remove({id: application.id}, callback);
    });
  },

  marketDAOClient: function marketDAOClient(callback) {
    DatabaseDAO.getClient('Marketplace', callback);
  },

  marketDAOHistoryClient: function marketDAOHistoryClient(callback) {
    DatabaseDAO.getClient('HistoryMarketplace', callback);
  },

  generateServiceSchema: function generateServiceSchema(object, callback) {
    async.waterfall([
      // generate schema skeleton
      function (next) {
        var serviceSchema = {
          "$schema": "http://json-schema.org/draft-04/schema#",
          "id": "service.json",
          "title": "WebGAP Service",
          "description": "Fill the form below",
          "type": "object",
          "properties": {
            "params": {
              "type": "object",
              "properties": {}
            },
            "redirects": {
              "type": "object",
              "properties": {}
            }
          }
        };
        next(serviceSchema);
      },
      // generate structure from parameters
      function (serviceSchema, next) {
        if (object.run) {
          for (var i = 0; i < object.run.params.length; i++) {
            serviceSchema.properties.params.properties[object.run.params[i].options.name] = {};
            serviceSchema.properties.params.properties[object.run.params[i].options.name].type = object.run.params[i].options.type;
            serviceSchema.properties.params.properties[object.run.params[i].options.name].format = object.run.params[i].options.format;
            serviceSchema.properties.params.properties[object.run.params[i].options.name].pattern = object.run.params[i].options.pattern;
            serviceSchema.properties.params.properties[object.run.params[i].options.name].additionalProperties = false;
          }
          return next(serviceSchema);
        } else {
          return callback(new Error(), null);
        }
      },
      // generate structure from arguments
      function (serviceSchema) {
        if (object.run) {
          for (var i = 0; i < object.run.redirects.length; i++) {
            serviceSchema.properties.redirects.properties[object.run.redirects[i].options.name] = {};
            serviceSchema.properties.redirects.properties[object.run.redirects[i].options.name].type = object.run.redirects[i].options.type;
            serviceSchema.properties.redirects.properties[object.run.redirects[i].options.name].format = object.run.redirects[i].options.format;
            serviceSchema.properties.redirects.properties[object.run.redirects[i].options.name].pattern = object.run.redirects[i].options.pattern;
            serviceSchema.properties.redirects.properties[object.run.redirects[i].options.name].additionalProperties = false;
          }
          return callback(serviceSchema);
        } else {
          return callback(new Error(), null);
        }
      }
    ]);
  }
};
