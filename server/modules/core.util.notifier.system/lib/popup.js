/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-03-2015
 *
 */

/**
 * Notification goes into the user session:
 * - The request is used to get the session manager and place a new item 'message'
 * - The message should be a valid 'Notifier.Notification.Message'
 *
 * @param {object} options the options for the current notification system
 *        {object] options.request the session request containing a '_passport' object
 *        {String] options.message the message to publish
 * @param {function} callback
 */
module.exports = {
  
  NAME: 'POPUP',

  notify: function (options, callback) {
    // callback is not mandatory - the approach of messaging systems
    callback = callback || function noop() {
      };
    // validate params
    if (!options || !options.request) {
      return callback(new Error('Missing required params: {request, message}.'));
    }
    // validate session
    if (!options.request._passport || !options.request._passport.session) {
      return callback(new Error('Cannot deliver message. Param request does not contain a valid \'passport\' session.'));
    }
    console.info('Sending message using [%s].', this.NAME);
    options.request._passport.session.message = options.message;
    return callback();
  }
};