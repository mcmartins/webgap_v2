/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 08-08-2014
 *
 */

var fs = require('fs');
var http = require('http');
var https = require('https');
var helmet = require('helmet');
var express = require('express');
var tooBusy = require('toobusy-js');
var compress = require('compression');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressValidator = require('express-validator');
var configuration = require('@webgap/configuration');
var notifier = require('@webgap/notifier');
var app = express();
var router = express.Router();

/**
 * Main Module
 * Contains Handlers for Server specific Modules
 *
 * @constructor
 */
function CommonModule() {

  console.info('Initializing Web Module...');

  app.enable('jsonp callback');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(expressValidator());
  app.use(cookieParser());
  app.use(compress({threshold : 100}));
  app.use(helmet.hsts({
    maxAge: 10886400000,
    includeSubdomains: true,
    preload: true
  }));
  app.use(helmet.xssFilter());
  app.use(helmet.frameguard());
  app.use(helmet.hidePoweredBy());
  app.use(helmet.ieNoOpen());
  app.use(helmet.noSniff());
  //app.use(helmet.crossdomain()); // validate in production

  //TODO add maxlag to configuration
  // middleware which blocks requests when server is too busy
  console.info('busy middleware is set to %s ms', tooBusy.maxLag(5000));
  app.use(function (req, res, next) {
    tooBusy() ? res.status(503).json({status: 'BUSY'}) : next();
  });

  // middleware enable CORS - cross origin resource sharing
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });

  // middleware for error handling
  app.use(function (err, req, res, next) {
    if (err) {
      notifier.notify({
        request: req,
        notification: {
          userId: options.request.user.id,
          message: {
            key: 'messages.error.internal-server-error',
            data: err
          }
        }
      }, function handleErrors(err) {
        if (err) {
          console.error('Something went wrong with the notification: %s\n%s', JSON.stringify(options.message), err);
        }
      });
    }
    next();
  });

  var server;
  if (configuration.get('SERVER.PROTOCOL').indexOf('https') > -1) {
    var options = {
      key: fs.readFileSync(configuration.get('SERVER.CERTIFICATES_PATH') + '/ssl.key'),
      cert: fs.readFileSync(configuration.get('SERVER.CERTIFICATES_PATH') + '/ssl.crt')
    };
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }

  // USING GLOBAL EXPLICITLY FOR EXPRESS APP AND ROUTER
  global.express = {};
  global.express.app = app;
  global.express.router = router;

  startServer(server);

  process.nextTick(function () {
    addDefaultRoutes();
    startTerminationHandlers();
  });
}

/**
 *  Setup termination handlers
 */
function startTerminationHandlers() {

  process.on('exit', function () {
    console.info('Node server stopped.');
  });

  ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1',
    'SIGSEGV', 'SIGUSR2', 'SIGTERM'].forEach(function (element) {
    process.on(element, function () {
      console.info('Received [%s] terminating server...', element);
      process.exit();
    });
  });
}

/**
 * Default routes 
 */
function addDefaultRoutes() {

  /**
   * Handles all contexts
   * 404 not found (ALWAYS Keep this as the last route)
   */
  router.get('/*', function (req, res) {
    res.redirect('/not-found');
  });

  app.use('/', router);
}


/**
 * Start http(s) server 
 */
function startServer(server) {

  server.listen(configuration.get('SERVER.PORT'), configuration.get('SERVER.IP'), function () {
    console.info('Portal web server started on [%s%s:%s]...',
      configuration.get('SERVER.PROTOCOL'),
      configuration.get('SERVER.HOST'), configuration.get('SERVER.APACHE_PORT'));
  });
}

module.exports = CommonModule;