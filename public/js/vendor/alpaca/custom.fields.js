$.alpaca.Fields.DataFromURL = $.alpaca.Fields.SelectField.extend({

  /**
   * The url to invoke and return data
   *
   * @returns {string}
   */
  getUrl: function () {
    return "/invalid-url";
  },

  /**
   * @see Alpaca.Fields.Field#setup
   */
  setup: function () {

    this.schema["enum"] = [];
    this.schema["enum"].push('none');
    this.options.optionLabels = [];
    this.options.optionLabels.push('None');
    var self = this;
    // TODO load users by invoking an url using an authentication token
    // the same approach can be used to load anything form the system to create custom fields / other components
    // FIXME this sync code should be changed to async - ALPACA does not seem to handle it
    $.ajax({ url: self.getUrl(),
      async: false,
      dataType: 'json',
      success: function(data) {
        if (data) {
          data.forEach(function (elem) {
            self.schema["enum"].push(elem.id);
            self.options.optionLabels.push(elem.name);
          });
        }
      }
    });
    self.base();
  }

});

Alpaca.registerFieldClass("DataFromURL", Alpaca.Fields.DataFromURL);

$.alpaca.Fields.UserField = $.alpaca.Fields.DataFromURL.extend({

  /**
   * {@inheritDoc}
   */
  getUrl: function () {
    return "/internal/users/list";
  },

  /**
   * @see Alpaca.Field#getFieldType
   */
  getFieldType: function () {
    return "user";
  },

  /**
   * @see Alpaca.Fields.TextField#getTitle
   */
  getTitle: function () {
    return "User Field";
  },

  /**
   * @see Alpaca.Fields.TextField#getDescription
   */
  getDescription: function () {
    return "Provides a dropdown selector of all users available in the system.";
  }

});

Alpaca.registerFieldClass("user", Alpaca.Fields.UserField);
Alpaca.registerDefaultFormatFieldMapping("user", "user");